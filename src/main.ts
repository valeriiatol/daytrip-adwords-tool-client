
window.onload = () =>
{
webix.ui(
    {
      cols:[
        {
          width: 200,
          minHeight: 820,
          rows:[
            { view:"template", type:"header", template:"Menu" },
            {  view:"tree", select:true, id: "menu_tree", on: {
        onSelectChange:function () {
          var nodeId = $$("menu_tree").getSelectedId();
          if(nodeId == 1){
              loadCampaignTemplates();
            }
          if(nodeId == 2){
              loadDestinationMapping();
          }
        }
      },
      data: [
        { id:"1", value:"Campains"},
        { id:"2", value:"Destinations mapping"}
      ]
    }]},
        {
          id: "layout",
          sizeToContent:true,
          /*padding:100,*/
          rows:[
          { view:"template",
            type:"header", template:"Campaign template" }]
        }
      ]
    ]
  });


  loadCampaignTemplates();
}

var currentAdGroupIndex = 0;
var currentAdIndex = 0;
var adList = [];

function loadCampaignTemplates(){
  webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/templates", function(data){
    if(data != undefined && data != null){
      var objectData = JSON.parse(data);
      var jsonData = [];
      for(var i = 0; i < objectData.length; i++){
        var item = {};
        item["id"] = objectData[i].id;
        item["title"] = objectData[i].name;
        item["value"] = objectData[i].name;
        jsonData.push(item);
      }

      if($$("campaign_template") != undefined){
        $$("layout").removeView($$("campaign_template"));
      }
      if($$("campaign_templates") != undefined){
        $$("layout").removeView($$("campaign_templates"));
      }
      if($$("mapping_layout") != undefined){
        $$("layout").removeView($$("mapping_layout"));
      }

      $$("layout").addView(new CampaignsListTemplate().getSpecification(JSON.stringify(jsonData)));
    }
  });
}

function loadDestinationMapping(){
   webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/mapCampaignsToRoutes", function(data){
   if(data != undefined && data != null){
      var objectData = JSON.parse(data);
      //We need to add property "value" for each item of campaigns for dropdown to be displayed correctly
      if(objectData.campaigns != null && objectData.campaigns != undefined){
        var campaigns = [];
        for(var i = 0; i < objectData.campaigns.length; i++ )
        {
            var campaign = {id: objectData.campaigns[i].id, name:objectData.campaigns[i].name, value: objectData.campaigns[i].name };
            campaigns.push(campaign);
        }
        objectData.campaigns = campaigns;
      }
      if(objectData.routes != undefined && objectData.routes != null){
        var routes = [];
        for(var i = 0; i < objectData.routes.length; i++ )
        {
            var route = {id: objectData.routes[i].id+'-'+objectData.routes[i].originid, origin: objectData.routes[i].origin, destination: objectData.routes[i].destination };
            routes.push(route);
        }
        objectData.routes = routes;
      }

      if($$("campaign_template") != undefined){
        $$("layout").removeView($$("campaign_template"));
      }
      if($$("campaign_templates") != undefined){
        $$("layout").removeView($$("campaign_templates"));
      }

      $$("layout").addView(new MappingCampaignsToRoutesTemplate().getSpecification(objectData));
    }
  });
}

function submit(){
  var formValues = $$("campaign_template").getValues();
  alert(JSON.stringify(formValues));
  webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/submitTemplate", formValues, function(data){
    alert(data);
    var model = JSON.parse(data);
    //campaign data
    if(model.campaign != undefined && model.campaign != null){
          adsToList(model.ads);
          //Update id
          (<HTMLInputElement>document.getElementById("id")).value = model.campaign.id;
    }

    //Rerender adgroups for ads list update
    rerenderAdGroups(model);
    rerenderAds(model);
  });
}

function adsToList(ads){
  adList = [];
  for(var i = 0; i< ads.length; i++){
    var adItem = {id:ads[i].id, value:ads[i].name};
    adList.push(adItem);
  }
}

function addAdGroup(){
  $$("campaign_template").addView(new AdgroupTemplate().getSpecification(currentAdGroupIndex,adList));
}

function addAd(){
  $$("campaign_template").addView(new AdTemplate().getSpecification(currentAdIndex));
  currentAdIndex++;
}


function removeAdGroup(button_id){
  var result = confirm("Do you really want to delete ad group?");
  if (result) {
    var index = button_id.substring(14);
    var adgroupIndex = "adgroup" + index;
    if($$("adgroup_id" + index) != undefined){
      var adGroupId = $$("adgroup_id" + index).getValue();
      var post = { adGroupId: adGroupId};
      webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteAdGroup",post , function(text, data, xhr){
        //alert(text);
      });
    }
  $$("campaign_template").removeView(adgroupIndex);
  }
}

function removeAd(button_id){
  var result = confirm("Do you really want to delete ad?");
  if (result) {
    var index = button_id.substring(9);
    var adIndex = "ad" + index;
    if($$("ad_id" + index) != undefined){
      var adId = $$("ad_id" + index).getValue();
      var post = { adId: adId};
      webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteAd",post , function(data){
        var model = JSON.parse(data);
        //campaign data
        if(model.campaign != undefined && model.campaign != null){
              adsToList(model.ads);
        }
        //Rerender adgroups for ads list update
        rerenderAdGroups(model);
        rerenderAds(model);
      });
    }
  $$("campaign_template").removeView(adIndex);
  }
}

function createCampaign(){
  load(0);
}

function load(templateId){
    webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/template?id="+templateId, function(data){
      $$("layout").removeView("campaign_templates");
      $$("layout").addView(new CampaignTemplate().getSpecification());
      loadTemplateModel(data, templateId);
    });
    $$("menu_tree").unselectAll();
  }

  function loadTemplateModel(jsonData, templateId){
    var model = JSON.parse(jsonData);
    //campaign data
    if(model.campaign != undefined && model.campaign != null && ((templateId != undefined && templateId > 0) || templateId == undefined)){
      (<HTMLInputElement>document.getElementById("campaign_name")).value = model.campaign.name;
      (<HTMLInputElement>document.getElementById("budget")).value = model.campaign.budget;
      (<HTMLInputElement>document.getElementById("id")).value = model.campaign.id;
      (<HTMLInputElement>document.getElementById("ad_extensions")).value = model.campaign.ad_extensions;
    }
    //ads
    loadAds(model);

    //AdGroups
    loadAdGroups(model);
  }

  function loadAdGroups(parsedData){
      if(parsedData.adGroups == undefined || parsedData.adGroups == null){
          $$("campaign_template").addView(new AdgroupTemplate().getSpecification(currentAdGroupIndex, adList));
          currentAdGroupIndex++;
    }
    else{
      rerenderAdGroups(parsedData);
      currentAdGroupIndex = parsedData.adGroups.length;
    }
  }

  //Used for rerendering adgroups views onload or when list of ads was changed
  function rerenderAdGroups(parsedData){
    for(var i = 0; i < parsedData.adGroups.length; i++){
      var group = parsedData.adGroups[i];
      var adsIds: Array<any> = JSON.parse(group.ad_ids);
      if(adsIds != undefined && adsIds != null && adsIds.length > 0){
        group.ad_ids = adsIds.join(",");
      }
      $$("campaign_template").removeView("adgroup" + i);
      $$("campaign_template").addView(new AdgroupTemplate().getFilledSpecification(group, i, adList));
    }
  }

  function rerenderAds(parsedData)
  {
    for(var i = 0; i < currentAdIndex; i++)
    {
      $$("campaign_template").removeView("ad" + i);
    }
    loadAds(parsedData);
  }

  function loadAds(parsedData){
    if(parsedData.ads == undefined || parsedData.ads == null){
      $$("campaign_template").addView(new AdTemplate().getSpecification(currentAdIndex));
      currentAdIndex++;
    }
    else{
      for(var i = 0; i < parsedData.ads.length; i++){
        $$("campaign_template").addView(new AdTemplate().getFilledSpecification(parsedData.ads[i], i));
      }
      currentAdIndex = parsedData.ads.length;
      adsToList(parsedData.ads);
    }
  }

  function removeCampaign()
  {
    var templateId = $$("templates_list").getSelectedId();
    if(templateId != "")
    {
      if(confirm("Do you want to delete this campaign?"))
      {
        var post = { id: templateId};
        webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteCampaign",post , function(){
          $$("layout").removeView($$("campaign_templates"));
          loadCampaignTemplates();
      });
    }
  }
}

  function generateCsvMapping(){
    var campaignId = $$("campaigns_select").getValue();
    var routesSelected = $$("routes_table").getSelectedId();
    var routeIds = [];
    var url = "http://campaigns.ondaytrip.com/campaigns/generateCsv?"+ "campaignId=" + campaignId;

    if(routesSelected.length != undefined)
    {
      for(var i = 0; i < routesSelected.length; i++){
        url += "&routeIds[]=" + routesSelected[i].id;
        routeIds.push(routesSelected[i].id);
      }
    }
    else
    {
      url += "&routeIds[]=" + routesSelected.id;
      routeIds.push(routesSelected.id);
    }

    window.open(url);
  }
