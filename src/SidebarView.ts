class SidebarView implements IView
{
    public getSpecification(): any
    {
        return {
            view: "sidebar",

            click: this.onClick,

            data: [
                { id: "dashboard", icon: "line-chart", value: "Dashboard" },

                { id: "infrastructure", icon: "terminal", value: "Infrastructure" },

                { id: "content", icon: "file", value: "Content", data: [
                    { id: "locations", value: "Locations" },
                    { id: "routes", value: "Routes" },
                ]},

                { id: "businessDevelopment", icon: "building", value: "Business development", data: [
                    { id: "affiliatePartners", value: "Affiliate partners" }
                ]},

                { id: "onlineCampaigns", icon: "globe", value: "Online campaigns", data: [
                    { id: "searchAds", value: "Search ads" },
                    { id: "searchCampaigns", value: "Search campaigns" }
                ]},

                { id: "operations", icon: "car", value: "Operations", data: [
                    { id: "guides", value: "Guides" },
                    { id: "unclaimedTrips", value: "Unclaimed trips" },
                    { id: "allTrips", value: "All trips" }
                ]},

                { id: "crm", icon: "group", value: "Customer relations"}
            ]
        };
    }

    public onClick(id: string)
    {
        //alert(id);
    }
}
