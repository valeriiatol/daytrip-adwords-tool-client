class AdTemplate
{
    public getSpecification(index): any
    {
        return {
          on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "ad" + index,
            cols:[
           {
           rows:[
             { view: "label", label: "Ad Name" },
             { view: "text", name: "ad_name" + index },
             { view: "label", label: "Headline 1" },
             { view: "text", name: "ad_headline_one" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:30 } },
             { view: "label", label: "Headline 2" },
             { view: "text", name: "ad_headline_two" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:30 } }
           ]},
           {
           rows:[
              { view: "label", label: "Description" },
              { view: "textarea", name: "ad_description" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:80 } }
           ]
         },
         { rows: [
           {view: "button", label: "Remove", click: removeAd, inputWidth: 120, id: "removeAd_"+index}
         ]
       }
       ],
            autoheight: true
        };
    }

    public getFilledSpecification(ad, index): any
    {
        return {
          on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "ad" + index,
            cols:[
           {
           rows:[
             { view: "label", label: "Ad Name" },
             { view: "text", name: "ad_name" + index, value:ad.name },
             { view: "label", label: "Headline 1" },
             { view: "text", name: "ad_headline_one" + index, value: ad.headline_1, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:30 } },
             { view: "label", label: "Headline 2" },
             { view: "text", name: "ad_headline_two" + index, value: ad.headline_2, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:30 } },
             { view: "text",  name: "ad_id" + index, type: "hidden", value: ad.id}
           ]
           },
           {
             rows:[
                { view: "label", label: "Description" },
                { view: "textarea", name: "ad_description" + index, value: ad.description, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes:{ maxlength:80 } }
             ]
         },
         { rows: [
           {view: "button", label: "Remove", click: removeAd, inputWidth: 120, id: "removeAd_"+index}
         ]
       }
     ],
            autoheight: true
        };
    }


    public onItemDblClick(id: number)
    {
        alert(id);
    }

    public onStructureLoad()
    {
        var instance: webix.ui.datatable = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    }
}
