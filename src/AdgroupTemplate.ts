class AdgroupTemplate
{
    public getSpecification(index, ads): any
    {
        return {
          on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "adgroup" + index,
            cols:[
           {
           rows:[
             { view: "label", label: "Adgroup name" },
             { view: "text", name: "adgroup_name" + index },
             { view: "label", label: "Bid" },
             { view: "text", name: "adgroup_bid" + index, placeholder: "Ex. 0.9" },
             { view: "label", label: "Landing page" },
             { view: "text", name: "adgroup_landingpage" + index, placeholder: "Ex. https://ondaytrip.com/{from}-to-{to}" },
             { view: "label", label: "Display Url"},
             { view: "text", name: "adgroup_displayurl" + index},
             { view:"multiselect", label:"Ads", name: "adgroup_ads" + index, labelWidth:100,
             options: ads }
           ]},
           {
           rows:[
             { view: "label", label: "Keywords" },
             { view: "textarea", name: "adgroup_keywords" + index, placeholder: "Put {from}, {to} if needed. Write whole keyword in [] as exact, in \"\" as phrase. Use , separator " }
           ]
           },
           {
           rows:[
             { view: "label", label: "Negative Keywords" },
             { view: "textarea", name: "adgroup_negative_keywords" + index, placeholder: "Put {from}, {to} if needed. Write whole keyword in [] as exact, in \"\" as phrase. Use , separator "}
           ]
         },
         { rows: [
           {view: "button", label: "Remove", click: removeAdGroup, inputWidth: 120, id: "removeAdGroup_"+index}
         ]
       }
           ],
            autoheight: true
        };
    }

    public getFilledSpecification(adGroup, index, ads){
      return {
        on: {
              onItemDblClick: this.onItemDblClick,
              onStructureLoad: this.onStructureLoad
          },
          view: "layout",
          id: "adgroup" + index,
          cols:[
         {
         rows:[
           { view: "label", label: "Adgroup name" },
           { view: "text", name: "adgroup_name" + index, value: adGroup.name },
           { view: "label", label: "Bid" },
           { view: "text", name: "adgroup_bid" + index, value: adGroup.bid },
           { view: "label", label: "Landing page" },
           { view: "text", name: "adgroup_landingpage" + index, value: adGroup.landing_url },
           { view: "label", label: "Display Url"},
           { view: "text", name: "adgroup_displayurl" + index, value: adGroup.display_url},
           { view: "text",  name: "adgroup_id" + index, type: "hidden", value: adGroup.id},
           { view:"multiselect", label:"Ads", name: "adgroup_ads" + index, labelWidth:100,
           options: ads,  value: adGroup.ad_ids }
          ]
         },
         {
         rows:[
           { view: "label", label: "Keywords" },
           { view: "textarea", name: "adgroup_keywords" + index, value: JSON.parse(adGroup.keywords) }
         ]
         },
         {
         rows:[
           { view: "label", label: "Negative Keywords" },
           { view: "textarea", name: "adgroup_negative_keywords" + index, value: JSON.parse(adGroup.negative_keywords) }
         ]
         },
         { rows: [
           {view: "button", label: "Remove", click: removeAdGroup, inputWidth: 120, id: "removeAdGroup_"+index}
         ]
       }
         ],
          autoheight: true
      };
    }

    public onItemDblClick(id: number)
    {
        alert(id);
    }

    public onStructureLoad()
    {
        var instance: webix.ui.datatable = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    }
}
