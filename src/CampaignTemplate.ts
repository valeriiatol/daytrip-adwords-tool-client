class CampaignTemplate
{
    public getSpecification(): any
    {
        return{
          view: "form",
            id: "campaign_template",
            scroll: "y",
            elements:[
              {
                  view: "toolbar",
                  cols:
                  [
                      { view: "label", label: "daytrip campaigns" }
                  ]
               },
               {
                   cols:
                   [
                       {
                         rows:
                           [
                               { view: "label", label: "Campaign name" },
                               { view: "text", name: "campaign_name" },
                               { view: "label", label: "Budget" },
                               { view: "text", name: "budget", placeholder: "Ex. 0.9"  },
                               { view: "label", label: "Ad extensions" },
                               { view: "text", name: "ad_extensions" },
                               { view: "text",  name: "id", type: "hidden"},
                               {   cols:
                                 [
                                   {view:"button", type:"iconButton", icon: "plus-circle", label: "Add new adgroup", inputWidth: 170, click: addAdGroup},
                                   {view: "button", label: "Add new ad", click: addAd, inputWidth: 170,type:"iconButton", icon: "plus-circle"}
                                 ]
                               },
                               { view:"button", value:"Save campaign" , type:"submit", click:submit, id: "saveCampaignButton", inputWidth: 270}
                           ]
                       }
                   ]
               }
           ]
        }
    }
}
