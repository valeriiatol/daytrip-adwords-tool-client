class AllLocationsView implements IView
{
    public getSpecification(): any
    {
        return {
            id: "locations_datatable",
            view: "datatable",
            select: "row",

            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },

            columns: [
                { id: "id", header: "ID",  },
                { id: "title", header: "Title" },
                { id: "score", header: "Score" },
                { id: "country", header: "Country" },
                { id: "is_live", header: "Live" },
                { id: "is_destination", header: "Destination" },
                { id: "is_location", header: "Location" },
                { id: "to_review", header: "To review" },
                { id: "has_image", header: "Image" },
                { id: "has_copy", header: "Copywriting" },
            ]
        };
    }

    public onItemDblClick(id: number)
    {
        alert(id);
    }

    public onStructureLoad()
    {
        var instance: webix.ui.datatable = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    }
}
