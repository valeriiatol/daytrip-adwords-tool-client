class MappingCampaignsToRoutesTemplate{

  public getSpecification(model): any
  {
      return{
        view: "form",
          id: "mapping_layout",
          height: "100%",
          scroll: "y",
          elements:[
             {
               rows:[
               {cols:
                 [
                   { view:"select", label:"Campaigns", name: "campaigns_select", labelWidth:100, height: "100%", options: model.campaigns},
                   { view:"datatable", labelHeight: "100%", resizeColumn:true, resizeRow:true, id: "routes_table", select:"row", multiselect:true,
                   columns:[
                     { id: "origin", header: "Origin", sort: "string"},
                     {id: "destination", header: "Destination", sort:"string"}
                   ],
                   data: JSON.stringify(model.routes)
                 }
               ]},
               { view: "button", label: "Generate CSV", inputWidth: "150", align: "right", click: "generateCsvMapping"}]
}]
}
}
}
