class CampaignsListTemplate
{
    public getSpecification(templates): any
    {
        return{
          id: "campaign_templates",
          rows:[
            {view: "button", label: "Create new campaign", click: createCampaign, inputWidth: 190, align:"right"},
            {view: "button", label: "Remove campaign", inputWidth: 190, align:"right", click: removeCampaign},
            {view: "list",
            id: "templates_list",
            data: templates,
            select:true,
            on:{
              "onItemDblClick":function(id, e, trg){
                var templateId = $$("templates_list").getSelectedId();
                load(templateId);
              }
            }
          }
          ]

        }
    }
}
