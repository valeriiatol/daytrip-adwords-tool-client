var AdgroupTemplate = (function () {
    function AdgroupTemplate() {
    }
    AdgroupTemplate.prototype.getSpecification = function (index, ads) {
        return {
            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "adgroup" + index,
            cols: [
                {
                    rows: [
                        { view: "label", label: "Adgroup name" },
                        { view: "text", name: "adgroup_name" + index },
                        { view: "label", label: "Bid" },
                        { view: "text", name: "adgroup_bid" + index, placeholder: "Ex. 0.9" },
                        { view: "label", label: "Landing page" },
                        { view: "text", name: "adgroup_landingpage" + index, placeholder: "Ex. https://ondaytrip.com/{from}-to-{to}" },
                        { view: "label", label: "Display Url" },
                        { view: "text", name: "adgroup_displayurl" + index },
                        { view: "multiselect", label: "Ads", name: "adgroup_ads" + index, labelWidth: 100,
                            options: ads }
                    ] },
                {
                    rows: [
                        { view: "label", label: "Keywords" },
                        { view: "textarea", name: "adgroup_keywords" + index, placeholder: "Put {from}, {to} if needed. Write whole keyword in [] as exact, in \"\" as phrase. Use , separator " }
                    ]
                },
                {
                    rows: [
                        { view: "label", label: "Negative Keywords" },
                        { view: "textarea", name: "adgroup_negative_keywords" + index, placeholder: "Put {from}, {to} if needed. Write whole keyword in [] as exact, in \"\" as phrase. Use , separator " }
                    ]
                },
                { rows: [
                        { view: "button", label: "Remove", click: removeAdGroup, inputWidth: 120, id: "removeAdGroup_" + index }
                    ]
                }
            ],
            autoheight: true
        };
    };
    AdgroupTemplate.prototype.getFilledSpecification = function (adGroup, index, ads) {
        return {
            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "adgroup" + index,
            cols: [
                {
                    rows: [
                        { view: "label", label: "Adgroup name" },
                        { view: "text", name: "adgroup_name" + index, value: adGroup.name },
                        { view: "label", label: "Bid" },
                        { view: "text", name: "adgroup_bid" + index, value: adGroup.bid },
                        { view: "label", label: "Landing page" },
                        { view: "text", name: "adgroup_landingpage" + index, value: adGroup.landing_url },
                        { view: "label", label: "Display Url" },
                        { view: "text", name: "adgroup_displayurl" + index, value: adGroup.display_url },
                        { view: "text", name: "adgroup_id" + index, type: "hidden", value: adGroup.id },
                        { view: "multiselect", label: "Ads", name: "adgroup_ads" + index, labelWidth: 100,
                            options: ads, value: adGroup.ad_ids }
                    ]
                },
                {
                    rows: [
                        { view: "label", label: "Keywords" },
                        { view: "textarea", name: "adgroup_keywords" + index, value: JSON.parse(adGroup.keywords) }
                    ]
                },
                {
                    rows: [
                        { view: "label", label: "Negative Keywords" },
                        { view: "textarea", name: "adgroup_negative_keywords" + index, value: JSON.parse(adGroup.negative_keywords) }
                    ]
                },
                { rows: [
                        { view: "button", label: "Remove", click: removeAdGroup, inputWidth: 120, id: "removeAdGroup_" + index }
                    ]
                }
            ],
            autoheight: true
        };
    };
    AdgroupTemplate.prototype.onItemDblClick = function (id) {
        alert(id);
    };
    AdgroupTemplate.prototype.onStructureLoad = function () {
        var instance = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    };
    return AdgroupTemplate;
}());
var AdTemplate = (function () {
    function AdTemplate() {
    }
    AdTemplate.prototype.getSpecification = function (index) {
        return {
            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "ad" + index,
            cols: [
                {
                    rows: [
                        { view: "label", label: "Ad Name" },
                        { view: "text", name: "ad_name" + index },
                        { view: "label", label: "Headline 1" },
                        { view: "text", name: "ad_headline_one" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 30 } },
                        { view: "label", label: "Headline 2" },
                        { view: "text", name: "ad_headline_two" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 30 } }
                    ] },
                {
                    rows: [
                        { view: "label", label: "Description" },
                        { view: "textarea", name: "ad_description" + index, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 80 } }
                    ]
                },
                { rows: [
                        { view: "button", label: "Remove", click: removeAd, inputWidth: 120, id: "removeAd_" + index }
                    ]
                }
            ],
            autoheight: true
        };
    };
    AdTemplate.prototype.getFilledSpecification = function (ad, index) {
        return {
            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            view: "layout",
            id: "ad" + index,
            cols: [
                {
                    rows: [
                        { view: "label", label: "Ad Name" },
                        { view: "text", name: "ad_name" + index, value: ad.name },
                        { view: "label", label: "Headline 1" },
                        { view: "text", name: "ad_headline_one" + index, value: ad.headline_1, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 30 } },
                        { view: "label", label: "Headline 2" },
                        { view: "text", name: "ad_headline_two" + index, value: ad.headline_2, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 30 } },
                        { view: "text", name: "ad_id" + index, type: "hidden", value: ad.id }
                    ]
                },
                {
                    rows: [
                        { view: "label", label: "Description" },
                        { view: "textarea", name: "ad_description" + index, value: ad.description, placeholder: "Put {price}, {time}, {from}, {to} in text if needed", attributes: { maxlength: 80 } }
                    ]
                },
                { rows: [
                        { view: "button", label: "Remove", click: removeAd, inputWidth: 120, id: "removeAd_" + index }
                    ]
                }
            ],
            autoheight: true
        };
    };
    AdTemplate.prototype.onItemDblClick = function (id) {
        alert(id);
    };
    AdTemplate.prototype.onStructureLoad = function () {
        var instance = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    };
    return AdTemplate;
}());
var AllLocationsView = (function () {
    function AllLocationsView() {
    }
    AllLocationsView.prototype.getSpecification = function () {
        return {
            id: "locations_datatable",
            view: "datatable",
            select: "row",
            on: {
                onItemDblClick: this.onItemDblClick,
                onStructureLoad: this.onStructureLoad
            },
            columns: [
                { id: "id", header: "ID", },
                { id: "title", header: "Title" },
                { id: "score", header: "Score" },
                { id: "country", header: "Country" },
                { id: "is_live", header: "Live" },
                { id: "is_destination", header: "Destination" },
                { id: "is_location", header: "Location" },
                { id: "to_review", header: "To review" },
                { id: "has_image", header: "Image" },
                { id: "has_copy", header: "Copywriting" },
            ]
        };
    };
    AllLocationsView.prototype.onItemDblClick = function (id) {
        alert(id);
    };
    AllLocationsView.prototype.onStructureLoad = function () {
        var instance = $$("locations_datatable");
        instance.parse(webix.ajax("http://cp.ondaytrip.com/content/locations"));
    };
    return AllLocationsView;
}());
var CampaignsListTemplate = (function () {
    function CampaignsListTemplate() {
    }
    CampaignsListTemplate.prototype.getSpecification = function (templates) {
        return {
            id: "campaign_templates",
            rows: [
                { view: "button", label: "Create new campaign", click: createCampaign, inputWidth: 190, align: "right" },
                { view: "button", label: "Remove campaign", inputWidth: 190, align: "right", click: removeCampaign },
                { view: "list",
                    id: "templates_list",
                    data: templates,
                    select: true,
                    on: {
                        "onItemDblClick": function (id, e, trg) {
                            var templateId = $$("templates_list").getSelectedId();
                            load(templateId);
                        }
                    }
                }
            ]
        };
    };
    return CampaignsListTemplate;
}());
var CampaignTemplate = (function () {
    function CampaignTemplate() {
    }
    CampaignTemplate.prototype.getSpecification = function () {
        return {
            view: "form",
            id: "campaign_template",
            scroll: "y",
            elements: [
                {
                    view: "toolbar",
                    cols: [
                        { view: "label", label: "daytrip campaigns" }
                    ]
                },
                {
                    cols: [
                        {
                            rows: [
                                { view: "label", label: "Campaign name" },
                                { view: "text", name: "campaign_name" },
                                { view: "label", label: "Budget" },
                                { view: "text", name: "budget", placeholder: "Ex. 0.9" },
                                { view: "label", label: "Ad extensions" },
                                { view: "text", name: "ad_extensions" },
                                { view: "text", name: "id", type: "hidden" },
                                { cols: [
                                        { view: "button", type: "iconButton", icon: "plus-circle", label: "Add new adgroup", inputWidth: 170, click: addAdGroup },
                                        { view: "button", label: "Add new ad", click: addAd, inputWidth: 170, type: "iconButton", icon: "plus-circle" }
                                    ]
                                },
                                { view: "button", value: "Save campaign", type: "submit", click: submit, id: "saveCampaignButton", inputWidth: 270 }
                            ]
                        }
                    ]
                }
            ]
        };
    };
    return CampaignTemplate;
}());
var HeaderTemplate = (function () {
    function HeaderTemplate() {
    }
    HeaderTemplate.prototype.getSpecification = function (title) {
        return;
        {
            view: "template", type;
            "header", template;
            title;
        }
    };
    return HeaderTemplate;
}());
window.onload = function () {
    webix.ui({
        cols: [
            {
                width: 200,
                minHeight: 820,
                rows: [
                    { view: "template", type: "header", template: "Menu" },
                    { view: "tree", select: true, id: "menu_tree", on: {
                            onSelectChange: function () {
                                var nodeId = $$("menu_tree").getSelectedId();
                                if (nodeId == 1) {
                                    loadCampaignTemplates();
                                }
                                if (nodeId == 2) {
                                    loadDestinationMapping();
                                }
                            }
                        },
                        data: [
                            { id: "1", value: "Campains" },
                            { id: "2", value: "Destinations mapping" }
                        ]
                    }] },
            {
                id: "layout",
                sizeToContent: true,
                rows: [
                    { view: "template",
                        type: "header", template: "Campaign template" }]
            }
        ]
    });
    loadCampaignTemplates();
};
var currentAdGroupIndex = 0;
var currentAdIndex = 0;
var adList = [];
function loadCampaignTemplates() {
    webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/templates", function (data) {
        if (data != undefined && data != null) {
            var objectData = JSON.parse(data);
            var jsonData = [];
            for (var i = 0; i < objectData.length; i++) {
                var item = {};
                item["id"] = objectData[i].id;
                item["title"] = objectData[i].name;
                item["value"] = objectData[i].name;
                jsonData.push(item);
            }
            if ($$("campaign_template") != undefined) {
                $$("layout").removeView($$("campaign_template"));
            }
            if ($$("campaign_templates") != undefined) {
                $$("layout").removeView($$("campaign_templates"));
            }
            if ($$("mapping_layout") != undefined) {
                $$("layout").removeView($$("mapping_layout"));
            }
            $$("layout").addView(new CampaignsListTemplate().getSpecification(JSON.stringify(jsonData)));
        }
    });
}
function loadDestinationMapping() {
    webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/mapCampaignsToRoutes", function (data) {
        if (data != undefined && data != null) {
            var objectData = JSON.parse(data);
            if (objectData.campaigns != null && objectData.campaigns != undefined) {
                var campaigns = [];
                for (var i = 0; i < objectData.campaigns.length; i++) {
                    var campaign = { id: objectData.campaigns[i].id, name: objectData.campaigns[i].name, value: objectData.campaigns[i].name };
                    campaigns.push(campaign);
                }
                objectData.campaigns = campaigns;
            }
            if (objectData.routes != undefined && objectData.routes != null) {
                var routes = [];
                for (var i = 0; i < objectData.routes.length; i++) {
                    var route = { id: objectData.routes[i].id + '-' + objectData.routes[i].originid, origin: objectData.routes[i].origin, destination: objectData.routes[i].destination };
                    routes.push(route);
                }
                objectData.routes = routes;
            }
            if ($$("campaign_template") != undefined) {
                $$("layout").removeView($$("campaign_template"));
            }
            if ($$("campaign_templates") != undefined) {
                $$("layout").removeView($$("campaign_templates"));
            }
            $$("layout").addView(new MappingCampaignsToRoutesTemplate().getSpecification(objectData));
        }
    });
}
function submit() {
    var formValues = $$("campaign_template").getValues();
    alert(JSON.stringify(formValues));
    webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/submitTemplate", formValues, function (data) {
        alert(data);
        var model = JSON.parse(data);
        if (model.campaign != undefined && model.campaign != null) {
            adsToList(model.ads);
            document.getElementById("id").value = model.campaign.id;
        }
        rerenderAdGroups(model);
        rerenderAds(model);
    });
}
function adsToList(ads) {
    adList = [];
    for (var i = 0; i < ads.length; i++) {
        var adItem = { id: ads[i].id, value: ads[i].name };
        adList.push(adItem);
    }
}
function addAdGroup() {
    $$("campaign_template").addView(new AdgroupTemplate().getSpecification(currentAdGroupIndex, adList));
}
function addAd() {
    $$("campaign_template").addView(new AdTemplate().getSpecification(currentAdIndex));
    currentAdIndex++;
}
function removeAdGroup(button_id) {
    var result = confirm("Do you really want to delete ad group?");
    if (result) {
        var index = button_id.substring(14);
        var adgroupIndex = "adgroup" + index;
        if ($$("adgroup_id" + index) != undefined) {
            var adGroupId = $$("adgroup_id" + index).getValue();
            var post = { adGroupId: adGroupId };
            webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteAdGroup", post, function (text, data, xhr) {
            });
        }
        $$("campaign_template").removeView(adgroupIndex);
    }
}
function removeAd(button_id) {
    var result = confirm("Do you really want to delete ad?");
    if (result) {
        var index = button_id.substring(9);
        var adIndex = "ad" + index;
        if ($$("ad_id" + index) != undefined) {
            var adId = $$("ad_id" + index).getValue();
            var post = { adId: adId };
            webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteAd", post, function (data) {
                var model = JSON.parse(data);
                if (model.campaign != undefined && model.campaign != null) {
                    adsToList(model.ads);
                }
                rerenderAdGroups(model);
                rerenderAds(model);
            });
        }
        $$("campaign_template").removeView(adIndex);
    }
}
function createCampaign() {
    load(0);
}
function load(templateId) {
    webix.ajax().get("http://campaigns.ondaytrip.com/campaigns/template?id=" + templateId, function (data) {
        $$("layout").removeView("campaign_templates");
        $$("layout").addView(new CampaignTemplate().getSpecification());
        loadTemplateModel(data, templateId);
    });
    $$("menu_tree").unselectAll();
}
function loadTemplateModel(jsonData, templateId) {
    var model = JSON.parse(jsonData);
    if (model.campaign != undefined && model.campaign != null && ((templateId != undefined && templateId > 0) || templateId == undefined)) {
        document.getElementById("campaign_name").value = model.campaign.name;
        document.getElementById("budget").value = model.campaign.budget;
        document.getElementById("id").value = model.campaign.id;
        document.getElementById("ad_extensions").value = model.campaign.ad_extensions;
    }
    loadAds(model);
    loadAdGroups(model);
}
function loadAdGroups(parsedData) {
    if (parsedData.adGroups == undefined || parsedData.adGroups == null) {
        $$("campaign_template").addView(new AdgroupTemplate().getSpecification(currentAdGroupIndex, adList));
        currentAdGroupIndex++;
    }
    else {
        rerenderAdGroups(parsedData);
        currentAdGroupIndex = parsedData.adGroups.length;
    }
}
function rerenderAdGroups(parsedData) {
    for (var i = 0; i < parsedData.adGroups.length; i++) {
        var group = parsedData.adGroups[i];
        var adsIds = JSON.parse(group.ad_ids);
        if (adsIds != undefined && adsIds != null && adsIds.length > 0) {
            group.ad_ids = adsIds.join(",");
        }
        $$("campaign_template").removeView("adgroup" + i);
        $$("campaign_template").addView(new AdgroupTemplate().getFilledSpecification(group, i, adList));
    }
}
function rerenderAds(parsedData) {
    for (var i = 0; i < currentAdIndex; i++) {
        $$("campaign_template").removeView("ad" + i);
    }
    loadAds(parsedData);
}
function loadAds(parsedData) {
    if (parsedData.ads == undefined || parsedData.ads == null) {
        $$("campaign_template").addView(new AdTemplate().getSpecification(currentAdIndex));
        currentAdIndex++;
    }
    else {
        for (var i = 0; i < parsedData.ads.length; i++) {
            $$("campaign_template").addView(new AdTemplate().getFilledSpecification(parsedData.ads[i], i));
        }
        currentAdIndex = parsedData.ads.length;
        adsToList(parsedData.ads);
    }
}
function removeCampaign() {
    var templateId = $$("templates_list").getSelectedId();
    if (templateId != "") {
        if (confirm("Do you want to delete this campaign?")) {
            var post = { id: templateId };
            webix.ajax().post("http://campaigns.ondaytrip.com/campaigns/deleteCampaign", post, function () {
                $$("layout").removeView($$("campaign_templates"));
                loadCampaignTemplates();
            });
        }
    }
}
function generateCsvMapping() {
    var campaignId = $$("campaigns_select").getValue();
    var routesSelected = $$("routes_table").getSelectedId();
    var routeIds = [];
    var url = "http://campaigns.ondaytrip.com/campaigns/generateCsv?" + "campaignId=" + campaignId;
    if (routesSelected.length != undefined) {
        for (var i = 0; i < routesSelected.length; i++) {
            url += "&routeIds[]=" + routesSelected[i].id;
            routeIds.push(routesSelected[i].id);
        }
    }
    else {
        url += "&routeIds[]=" + routesSelected.id;
        routeIds.push(routesSelected.id);
    }
    window.open(url);
}
var MappingCampaignsToRoutesTemplate = (function () {
    function MappingCampaignsToRoutesTemplate() {
    }
    MappingCampaignsToRoutesTemplate.prototype.getSpecification = function (model) {
        return {
            view: "form",
            id: "mapping_layout",
            height: "100%",
            scroll: "y",
            elements: [
                {
                    rows: [
                        { cols: [
                                { view: "select", label: "Campaigns", name: "campaigns_select", labelWidth: 100, height: "100%", options: model.campaigns },
                                { view: "datatable", labelHeight: "100%", resizeColumn: true, resizeRow: true, id: "routes_table", select: "row", multiselect: true,
                                    columns: [
                                        { id: "origin", header: "Origin", sort: "string" },
                                        { id: "destination", header: "Destination", sort: "string" }
                                    ],
                                    data: JSON.stringify(model.routes)
                                }
                            ] },
                        { view: "button", label: "Generate CSV", inputWidth: "150", align: "right", click: "generateCsvMapping" }]
                }]
        };
    };
    return MappingCampaignsToRoutesTemplate;
}());
var SaveCampaignButtonView = (function () {
    function SaveCampaignButtonView() {
    }
    SaveCampaignButtonView.prototype.getSpecification = function (model) {
        return {
            id: "saveCampaignButton", view: "button", value: "Save campaign", type: "submit", click: submit, inputWidth: 270
        };
    };
    return SaveCampaignButtonView;
}());
var SidebarView = (function () {
    function SidebarView() {
    }
    SidebarView.prototype.getSpecification = function () {
        return {
            view: "sidebar",
            click: this.onClick,
            data: [
                { id: "dashboard", icon: "line-chart", value: "Dashboard" },
                { id: "infrastructure", icon: "terminal", value: "Infrastructure" },
                { id: "content", icon: "file", value: "Content", data: [
                        { id: "locations", value: "Locations" },
                        { id: "routes", value: "Routes" },
                    ] },
                { id: "businessDevelopment", icon: "building", value: "Business development", data: [
                        { id: "affiliatePartners", value: "Affiliate partners" }
                    ] },
                { id: "onlineCampaigns", icon: "globe", value: "Online campaigns", data: [
                        { id: "searchAds", value: "Search ads" },
                        { id: "searchCampaigns", value: "Search campaigns" }
                    ] },
                { id: "operations", icon: "car", value: "Operations", data: [
                        { id: "guides", value: "Guides" },
                        { id: "unclaimedTrips", value: "Unclaimed trips" },
                        { id: "allTrips", value: "All trips" }
                    ] },
                { id: "crm", icon: "group", value: "Customer relations" }
            ]
        };
    };
    SidebarView.prototype.onClick = function (id) {
    };
    return SidebarView;
}());
